//
//  WordPair.swift
//  Word Game
//
//  Created by Bsp-Hajizadeh on 4/4/1401 AP.
//

import Foundation

struct WordPair: Codable {
    let textEng, textSpa: String

    enum CodingKeys: String, CodingKey {
        case textEng = "text_eng"
        case textSpa = "text_spa"
    }
}
