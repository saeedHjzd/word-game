//
//  ViewController.swift
//  Word Game
//
//  Created by Bsp-Hajizadeh on 4/4/1401 AP.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var correctAttemptsCountLabel: UILabel!
    @IBOutlet weak var wrongAttemptsCountLabel: UILabel!
    @IBOutlet weak var spanishTranslationLabel: UILabel!
    @IBOutlet weak var englishWordLabel: UILabel!
    
    lazy var correctCounter: Int = 0 {
        didSet {
            correctAttemptsCountLabel.text = "\(correctCounter)"
        }
    }
    lazy var wrongCounter: Int = 0 {
        didSet {
            wrongAttemptsCountLabel.text = "\(wrongCounter)"
            if wrongCounter >= 3 {
                stopTheGame()
            }
        }
    }
    var wordsList: [WordPair] = []
    var timer: Timer = Timer()
    var presentedWordsCount = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadAndParseJsonFile()
        startTheGame()
    }
    
    fileprivate func stopTheGame() {
        stopAnimation()
        if timer.isValid {
            timer.invalidate()
        }
        showDialog()
    }
    
    fileprivate func presentNewWords() {
        presentedWordsCount += 1
        if presentedWordsCount > 15 {
            stopTheGame()
        }
        self.animateLabel()
        let randomWordIndex = Int.random(in: 0..<wordsList.count)
        englishWordLabel.text = wordsList[randomWordIndex].textEng
        var fourRandomSpanishWords: [String] = []
        fourRandomSpanishWords.append(wordsList[randomWordIndex].textSpa)
        fourRandomSpanishWords.append(wordsList[randomInt(from: 0, to: wordsList.count, exclude: randomWordIndex)].textSpa)
        fourRandomSpanishWords.append(wordsList[randomInt(from: 0, to: wordsList.count, exclude: randomWordIndex)].textSpa)
        fourRandomSpanishWords.append(wordsList[randomInt(from: 0, to: wordsList.count, exclude: randomWordIndex)].textSpa)
        spanishTranslationLabel.text = fourRandomSpanishWords[Int.random(in: 0..<fourRandomSpanishWords.count)]
    }

    fileprivate func processUserAnswer(isCorrect: Bool) {
        let engWord = englishWordLabel.text
        let spaWord = spanishTranslationLabel.text
        let result = wordsList.firstIndex(where: {$0.textEng == engWord }) == wordsList.firstIndex(where: {$0.textSpa == spaWord })
        if result == isCorrect {
            correctCounter += 1
        }else {
            wrongCounter += 1
        }
        resetTimer()
        presentNewWords()
    }
    @IBAction func correctTranslation(_ sender: UIButton) {
        processUserAnswer(isCorrect: true)
    }
    @IBAction func wrongTranslation(_ sender: UIButton) {
        processUserAnswer(isCorrect: false)
    }

}


extension ViewController {
    
    private func randomInt(from: Int, to: Int, exclude: Int) -> Int {
        var result = 0
        result = Int.random(in: from..<to-1)
        if result >= exclude {
            result += 1
        }
        return result
    }
    
    private func loadAndParseJsonFile() {
        if let path = Bundle.main.path(forResource: "words", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                wordsList = try JSONDecoder().decode([WordPair].self, from: data)
            } catch let error {
                print("*Failed to parse JSON : \(error)")
            }
        }else {
            print("Failed to load json file.")
        }
    }
    
    fileprivate func stopAnimation() {
        DispatchQueue.main.async {
            self.spanishTranslationLabel.layer.removeAllAnimations()
            self.view.layer.removeAllAnimations()
            self.view.layoutIfNeeded()
        }
    }
    
    private func resetTimer() {
        if timer.isValid {
            timer.invalidate()
        }
        timer = Timer.scheduledTimer(withTimeInterval: 5, repeats: true, block: { _ in
            if self.wrongCounter < 3 && self.presentedWordsCount < 15 {
                self.presentNewWords()
                self.wrongCounter += 1
            }else {
                self.stopTheGame()
            }
        })
    }
    
    private func animateLabel() {
        self.spanishTranslationLabel.frame = CGRect(x: 20, y: -30, width: self.view.frame.width - 40, height: 40)
        UIView.animate(withDuration: 5, delay: 0, options: .repeat) {
            self.spanishTranslationLabel.frame = CGRect(x: 20, y: self.view.frame.height, width: self.view.frame.width - 40, height: 40)
        }
    }
    
    private func showDialog() {
        let message = "Your final score is : \(self.correctCounter)"
        let alert = UIAlertController(title: "Game Over", message: message, preferredStyle: .alert)
        let exitGameAction = UIAlertAction(title: "Exit", style: .destructive) { _ in
            exit(0)
        }
        let resetGameAction = UIAlertAction(title: "Restart", style: .default) { _ in
            self.reStartTheGame()
        }
        alert.addAction(resetGameAction)
        alert.addAction(exitGameAction)
        self.present(alert, animated: true)
    }
    
    private func startTheGame(){
        DispatchQueue.main.async {
            self.animateLabel()
        }
        presentNewWords()
        
        if timer.isValid {
            timer.invalidate()
        }
        timer = Timer.scheduledTimer(withTimeInterval: 5, repeats: true, block: { _ in
            self.presentNewWords()
            self.wrongCounter += 1
        })
        spanishTranslationLabel.frame = CGRect(x: 20, y: -30, width: self.view.frame.width - 40, height: 40)
    }
    
    private func reStartTheGame() {
        correctCounter = 0
        wrongCounter = 0
        presentedWordsCount = 0
        startTheGame()
    }
    
}
